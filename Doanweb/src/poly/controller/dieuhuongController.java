package poly.controller;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import poly.entity.Main;

@Controller
@Transactional
public class dieuhuongController {
	@Autowired
	SessionFactory factory;
	
	
	@RequestMapping("ram")
	public String ram(ModelMap model){
		try {
			
		
		Session session = factory.getCurrentSession();
		String hql = "FROM Main where info.idtype = 1";
		Query query = session.createQuery(hql);
		List<Main> list = query.list();
		
		model.addAttribute("combo",list);
		model.addAttribute("title","Ram Giá Cực Cạnh Tranh");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "tab/combo";
	}
	
	@RequestMapping("chip")
	public String chip(ModelMap model){
		try {
			
		
		Session session = factory.getCurrentSession();
		String hql = "FROM Main where info.idtype = 2";
		Query query = session.createQuery(hql);
		List<Main> list = query.list();
		
		model.addAttribute("combo",list);
		model.addAttribute("title","CPU Giá Rẻ Mạnh Mẽ !");
		} catch (Exception e) {
			// TODO: handle exception
		}
		return "tab/combo";
	}
	@RequestMapping("main")
	public String mainn(ModelMap model){
		
		try {
			
			
			Session session = factory.getCurrentSession();
			String hql = "FROM Main where info.idtype = 3";
			Query query = session.createQuery(hql);
			List<Main> list = query.list();
			
			model.addAttribute("combo",list);
			model.addAttribute("title","Mainboard Giá Rẻ Mạnh Mẽ !");
			} catch (Exception e) {
				// TODO: handle exception
			}
		return "tab/combo";
		
	}
	
	@RequestMapping("vga")
	public String vga(ModelMap model){
		
		try {
			
			
			Session session = factory.getCurrentSession();
			String hql = "FROM Main where info.idtype = 4";
			Query query = session.createQuery(hql);
			List<Main> list = query.list();
			
			model.addAttribute("combo",list);
			model.addAttribute("title","Card Màn Hình Giá Rẻ Mạnh Mẽ !");
			} catch (Exception e) {
				// TODO: handle exception
			}
		return "tab/combo";
		
	}
	
	@RequestMapping("psu")
	public String psu(ModelMap model){
		
		try {
			
			
			Session session = factory.getCurrentSession();
			String hql = "FROM Main where info.idtype = 5";
			Query query = session.createQuery(hql);
			List<Main> list = query.list();
			
			model.addAttribute("combo",list);
			model.addAttribute("title","Nguồn - PSU Giá Rẻ Mạnh Mẽ !");
			} catch (Exception e) {
				// TODO: handle exception
			}
		return "tab/combo";
		
	}
	
	@RequestMapping("case")
	public String casee(ModelMap model){
		
		try {
			
			
			Session session = factory.getCurrentSession();
			String hql = "FROM Main where info.idtype = 6";
			Query query = session.createQuery(hql);
			List<Main> list = query.list();
			
			model.addAttribute("combo",list);
			model.addAttribute("title","Thùng - CASE Giá Rẻ Mạnh Mẽ !");
			} catch (Exception e) {
				// TODO: handle exception
			}
		return "tab/combo";
		
	}
	
	@RequestMapping("ocung")
	public String ocung(ModelMap model){
		
		try {
			
			
			Session session = factory.getCurrentSession();
			String hql = "FROM Main where info.idtype = 7";
			Query query = session.createQuery(hql);
			List<Main> list = query.list();
			
			model.addAttribute("combo",list);
			model.addAttribute("title","Ổ Cứng - Giá Rẻ Mạnh Mẽ !");
			} catch (Exception e) {
				// TODO: handle exception
			}
		return "tab/combo";
		
	}
	
	@RequestMapping("gear")
	public String gear(ModelMap model){
		
		try {
			
			
			Session session = factory.getCurrentSession();
			String hql = "FROM Main where info.idtype = 1008";
			Query query = session.createQuery(hql);
			List<Main> list = query.list();
			
			model.addAttribute("combo",list);
			model.addAttribute("title","Phụ Kiện - Giá Rẻ Mạnh Mẽ !");
			} catch (Exception e) {
				// TODO: handle exception
			}
		return "tab/combo";
		
	}
	

}
