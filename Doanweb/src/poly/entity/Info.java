package poly.entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Infos")
public class Info {
	@Id
	private Integer idtype;
	private String name;
	
	
	@OneToMany(mappedBy="info",fetch=FetchType.EAGER)
	private Collection<Main> mains;


	public Integer getIdtype() {
		return idtype;
	}


	public void setIdtype(Integer idtype) {
		this.idtype = idtype;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Collection<Main> getMains() {
		return mains;
	}


	public void setMains(Collection<Main> mains) {
		this.mains = mains;
	}

	

}
