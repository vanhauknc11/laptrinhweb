<%@ page pageEncoding = "utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html >
<html>
<head>
 <title>Login</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="./css/2.css">
  <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="./css/toastr.min.css">
<base href="${pageContext.servletContext.contextPath}/">
</head>
<body>


	<div class="container">

    <div class="khoi">
        <div class="logo">
        <a href="#"><img src="./img/logo.jpg" alt=""></a>
    </div>

    <div class="fom">
        
        <form action="login1.htm" method="POST" role="form">

            <div class="form-group">
                <label for="">Tài khoản</label>
                <input name="name" type="text" class="form-control" id="" placeholder="Tài khoản" required="required">
                <label for="">Mật khẩu</label>
                <input name="pass" type="password" class="form-control" id="" placeholder="Mật Khẩu" required="required">
            </div>
        
            
        
            <button type="submit" class="btn btn-success">ĐĂNG NHẬP</button>
        </form>
        <div class="register-link">
<!--             <p>Bạn chưa có tài khoản? <a href="#">Đăng ký</a></p>  -->
        </div>
        

    </div>
    </div>

</div>

<script src="./css/toastr.min.js"></script>
	<script>
		var x=${vl}
		if(x==1){
			toastr.options = {
					  "closeButton": true,
					  "debug": false,
					  "newestOnTop": false,
					  "progressBar": true,
					  "positionClass": "toast-top-right",
					  "preventDuplicates": false,
					  "onclick": null,
					  "showDuration": "300",
					  "hideDuration": "1000",
					  "timeOut": "5000",
					  "extendedTimeOut": "1000",
					  "showEasing": "swing",
					  "hideEasing": "linear",
					  "showMethod": "fadeIn",
					  "hideMethod": "fadeOut"
					}
			toastr["error"]("Tài khoản sai hoặc mật khẩu không chính xác")
		}
			
		
	</script>
</body>
</html>