<%@ page pageEncoding = "utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<!DOCTYPE html >
<html>
<head>
<title>Edit</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="./css/new.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/990909bc49.js"></script>
   <link rel="stylesheet" href="./css/toastr.min.css">
<base href="${pageContext.servletContext.contextPath}/">

</head>

<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-brand" >Admin Control</div>
    </div>
   <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Hello ${name}</a></li>
        <li><a href="logout.htm"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
 
</nav>

<div class="menu">
    
   
 


    
            <ul class="cha">
                 <li><i class="fas fa-tachometer-alt"></i></li>
         		<li > <a href="table.htm"><i class="fas fa-table"></i>Tổng Quan</a></li>
                <li ><a href="insert.htm"><i class="fas fa-plus-square"></i>Thêm Sản Phẩm</a></li>
                <li><a href="date.htm"><i class="fas fa-search"></i>Tra Cứu Đơn Hàng</a></li>
                <li><a href="search.htm"><i class="fas fa-chalkboard-teacher"></i>Thông Tin Khách Hàng</a></li>

            </ul>
        </div>
 

   
				<div class="phom">
				<h3>${message}</h3>
                   <form:form action="edit.htm" method="post" modelAttribute="main" enctype="multipart/form-data">
                               
                                <div class="form-group">
                                    <label for="example-email">Tên Sản Phẩm </label>
                                    <form:input required="required" path="name" type=""  class="form-control" placeholder="Ram cực xịn" />
                                </div>
                                  <div class="form-group">
                                    <form:input path="id" type="hidden"  class="form-control"  />
                                </div>
                                  <div class="form-group">
                                    <form:input path="img" type="hidden"  class="form-control"  />
                                </div>
                                <div class="form-group">
                                    <label for="example-email">Giá : </label>
                                    <form:input required="required" path="price" type="number" min="0" class="form-control" placeholder="1.000.000" />
                                </div>
                                <div class="form-group">
                                    <label for="example-email">Số Lượng : </label>
                                    <form:input required="required" path="amount" type="number" min="0" class="form-control" placeholder="4" />
                                </div>
                                <div class="form-group">
                                    <label for="example-email">Mô tả : </label>
                                    <form:input path="description" required="required" class="form-control" placeholder="Sản phẩm bao đẹp của ken" />
                                </div>
                                
                               
                                <div class="form-group">
                                    <label>Loại Sản Phẩm </label>
                                    <form:select class="form-control" path="info.idtype" items="${infos}" itemValue="idtype" itemLabel="name">
                                        
                                    </form:select>
                                </div>
                                <div class="form-group">
                                    <label>Upload image</label>
                                    <input type="file" name="photox" class="form-control" >
                                </div>
                                
                              
                                
                                <div class="form-group">
                                    
                                    <button type="submit" class="btn btn-success" >Submit</button>
                                </div>
                               
                            </form:form>



    

</div>


<script src="./css/toastr.min.js"></script>
	<script>
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		var x=${ed}
		if(x==1){
			toastr["success"]("Chỉnh sửa thành công !");
		}if(x==0){
			toastr["error"]("Chỉnh sửa thất bại ! ");
		}
		
	
			
		
	</script>

</body>
</html>