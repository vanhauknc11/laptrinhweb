<%@ page pageEncoding = "utf-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="f" %>

<!DOCTYPE html >
<html>
<head>
<title>Searching</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" href="./css/new.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
  <script src="https://kit.fontawesome.com/990909bc49.js"></script>
   <link rel="stylesheet" href="./css/toastr.min.css">
<base href="${pageContext.servletContext.contextPath}/">

</head>

<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <div class="navbar-brand" >Admin Control</div>
    </div>
   <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Hello ${name}</a></li>
        <li><a href="logout.htm"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      </ul>
 
</nav>

<div class="menu">
    
   
 


    
            <ul class="cha">
                <li><i class="fas fa-tachometer-alt"></i></li>
         		<li > <a href="table.htm"><i class="fas fa-table"></i>Tổng Quan</a></li>
                <li ><a href="insert.htm"><i class="fas fa-plus-square"></i>Thêm Sản Phẩm</a></li>
                <li class="active"><a href="date.htm"><i class="fas fa-search"></i>Tra Cứu Đơn Hàng</a></li>
                <li><a href="search.htm"><i class="fas fa-chalkboard-teacher"></i>Thông Tin Khách Hàng</a></li>

            </ul>
        </div>
 

   
				<div class="phom">
                          
                          <h3 class="text-center">Nhập Ngày , Tháng Cần Tra Cứu</h3>
							<form action="./checkdate.htm" method="post">
							
							
							<div class="input-group">
							<label for="basic-url">Ngày/Tháng/Năm</label>
							 
							  <input type="text" class="form-control" placeholder="01/10/2019" name="day" required="required" >
							</div>
							
							<div class="input-group">
							  <button type="submit" class="btn btn-success " style="width: 200%" >
							    Check 
							  </button>
							  </div>
							</form>
							
							
					<table class="table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">CMND</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="u" items="${ds}"> 
                                        <tr>
                                            <th scope="row">${u.idoder}</th>
                                            <td>${u.name}</td>
                                            <td><f:formatNumber minFractionDigits="0" value="${u.price}" > </f:formatNumber> đ </td>
                                            <td>${u.customer.cmnd}</td>
                                        </tr>
                                       </c:forEach>
                                    </tbody>
                                </table>			


</div>

<script src="./css/toastr.min.js"></script>
	<script>
	toastr.options = {
			  "closeButton": true,
			  "debug": false,
			  "newestOnTop": false,
			  "progressBar": true,
			  "positionClass": "toast-top-right",
			  "preventDuplicates": false,
			  "onclick": null,
			  "showDuration": "300",
			  "hideDuration": "1000",
			  "timeOut": "5000",
			  "extendedTimeOut": "1000",
			  "showEasing": "swing",
			  "hideEasing": "linear",
			  "showMethod": "fadeIn",
			  "hideMethod": "fadeOut"
			}
		var x=${nof}
		
			
		if(x==0){
			toastr["error"]("Không có đơn hàng nào tồn tại !");
		}else{
			toastr["success"]("Tìm thấy "+x+" đơn hàng !");
		}
		
	
			
		
	</script>

</body>
</html>